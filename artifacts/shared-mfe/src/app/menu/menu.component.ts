import { Component, OnInit } from '@angular/core';
import { MenuService } from 'shared-lib';

@Component({
	selector: 'app-default',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
	constructor(private menuService: MenuService) {}

	ngOnInit(): void {}

	onActionClick() {
		console.log('Shared Menu - Click Event');
		this.menuService.emitEvent('MODIFY');
	}
}
