import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: 'menu',
		loadChildren: () => import('./menu/menu.module').then((m) => m.MenuModule),
	},
	{
		path: 'header',
		loadChildren: () =>
			import('./header/header.module').then((m) => m.HeaderModule),
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
