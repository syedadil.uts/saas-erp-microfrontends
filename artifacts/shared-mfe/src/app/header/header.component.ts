import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'shared-lib';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
	constructor(private headerService: HeaderService) {}

	ngOnInit(): void {}

	onActionClick() {
		console.log('Shared Header - Click Event');
		this.headerService.emitEvent('IN_ACTIVE');
	}
}
