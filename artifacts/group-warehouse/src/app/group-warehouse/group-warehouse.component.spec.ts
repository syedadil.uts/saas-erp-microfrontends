import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupWarehouseComponent } from './group-warehouse.component';

describe('DefaultComponent', () => {
  let component: GroupWarehouseComponent;
  let fixture: ComponentFixture<GroupWarehouseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupWarehouseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupWarehouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
