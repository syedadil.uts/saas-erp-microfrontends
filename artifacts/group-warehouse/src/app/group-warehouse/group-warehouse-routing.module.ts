import { loadRemoteModule } from '@angular-architects/module-federation';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupWarehouseComponent } from './group-warehouse.component';

const routes: Routes = [
	{
		path: '',
		component: GroupWarehouseComponent,
		children: [
			{
				path: '',
				loadChildren: () =>
					loadRemoteModule({
						type: 'module',
						remoteEntry: 'http://localhost:1000/sharedRemoteEntry.js',
						exposedModule: './MenuModule',
					}).then((m) => m.MenuModule),
				outlet: 'shared-menu'
			},
			{
				path: '',
				loadChildren: () =>
					loadRemoteModule({
						type: 'module',
						remoteEntry: 'http://localhost:1000/sharedRemoteEntry.js',
						exposedModule: './HeaderModule',
					}).then((m) => m.HeaderModule),
				outlet: 'shared-header'
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class GroupWarehouseRoutingModule {}
