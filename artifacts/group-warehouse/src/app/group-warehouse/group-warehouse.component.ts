import { Component, OnInit } from '@angular/core';
import { HeaderService, MenuService } from 'shared-lib';

@Component({
	selector: 'app-default',
	templateUrl: './group-warehouse.component.html',
	styleUrls: ['./group-warehouse.component.scss'],
	providers: [MenuService, HeaderService],
})
export class GroupWarehouseComponent implements OnInit {
	color: string = '#000';

	constructor(
		private menuService: MenuService,
		private headerService: HeaderService
	) {}

	ngOnInit(): void {
		this.menuService.$events.subscribe((value) => {
			console.log('Group Warehouse - Intercepted Event', value);
			this.setRandomColor();
		});
		this.headerService.$events.subscribe((value) => {
			console.log('Group Warehouse - Intercepted Event', value);
			alert('Set Inactive');
		});
	}

	setRandomColor() {
		const letters = '0123456789ABCDEF';
		let color = '#';
		for (let i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		this.color = color;
	}
}
