import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupWarehouseRoutingModule } from './group-warehouse-routing.module';
import { GroupWarehouseComponent } from './group-warehouse.component';
import { ContainerModule } from '../container/container.module';


@NgModule({
  declarations: [
    GroupWarehouseComponent
  ],
  imports: [
    CommonModule,
    GroupWarehouseRoutingModule,
		ContainerModule
  ]
})
export class GroupWarehouseModule { }
