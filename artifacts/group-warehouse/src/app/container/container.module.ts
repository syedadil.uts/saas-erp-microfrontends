import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GwSharedMenuContainerComponent } from './gw-shared-mfe-container/gw-shared-mfe-container.component';
import { RouterModule } from '@angular/router';
import { GwSharedHeaderContainerComponent } from './gw-shared-header-container/gw-shared-header-container.component';

@NgModule({
	declarations: [
		GwSharedMenuContainerComponent,
		GwSharedHeaderContainerComponent,
	],
	imports: [CommonModule, RouterModule],
	exports: [GwSharedMenuContainerComponent, GwSharedHeaderContainerComponent],
})
export class ContainerModule {}
