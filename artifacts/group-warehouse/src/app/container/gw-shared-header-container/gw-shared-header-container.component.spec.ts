import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GwSharedHeaderContainerComponent } from './gw-shared-header-container.component';

describe('GwSharedHeaderContainerComponent', () => {
  let component: GwSharedHeaderContainerComponent;
  let fixture: ComponentFixture<GwSharedHeaderContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GwSharedHeaderContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GwSharedHeaderContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
