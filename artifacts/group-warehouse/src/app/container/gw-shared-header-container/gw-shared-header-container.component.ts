import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gw-shared-header-container',
  template: `
    <router-outlet name="shared-header"></router-outlet>
  `,
  styles: [
  ]
})
export class GwSharedHeaderContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
