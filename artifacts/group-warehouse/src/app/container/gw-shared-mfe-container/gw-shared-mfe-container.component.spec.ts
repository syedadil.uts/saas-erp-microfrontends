import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GwSharedMenuContainerComponent } from './gw-shared-mfe-container.component';

describe('GwSharedMfeContainerComponent', () => {
  let component: GwSharedMenuContainerComponent;
  let fixture: ComponentFixture<GwSharedMenuContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GwSharedMenuContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GwSharedMenuContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
