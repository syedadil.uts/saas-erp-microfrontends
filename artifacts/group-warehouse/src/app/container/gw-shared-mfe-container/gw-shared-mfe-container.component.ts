import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gw-shared-menu-container',
  template: `
    <router-outlet name="shared-menu"></router-outlet>
  `,
  styles: [
  ]
})
export class GwSharedMenuContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
