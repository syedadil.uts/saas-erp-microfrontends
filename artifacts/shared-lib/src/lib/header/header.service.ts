import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class HeaderService {
	$events: Observable<string>;

	private _controlSubject: Subject<string>;

	constructor() {
		this._controlSubject = new Subject();
		this.$events = this._controlSubject.pipe();
	}

	emitEvent(event: string) {
		console.log('Header Service - Emitting Event', event);
		this._controlSubject.next(event);
	}
}
